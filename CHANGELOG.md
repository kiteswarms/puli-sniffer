# Changelog

Until there is a dedicated guide how to use this log, please stick to this article: https://keepachangelog.com/de/0.3.0/
Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Fixed`, `Removed`

Before every entry put one of these to mark the severity of the change:
`Major`, `Minor` or `Patch`


## [Unreleased](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.5.0...master)

## [v2.5.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.4.1...v2.5.0) - 08.12.20
### Changed
- [Minor] Update to pulicast-python version 1.0
- [Patch] Fixed data hashing compatibility in combination with pulicast 1.0

## [v2.4.1](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.4.0...v2.4.1) - 08.12.20
### Added
- [Patch] Version check for pulicast-python dependency

## [v2.4.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.3.0...v2.4.0) - 11.11.20
### Added
- [Minor] Added state sniffer
### Changed
- [Patch] Changed default port to default pulicast port

## [v2.3.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.2.2...v2.3.0) - 05.10.20
### Changed
- [Patch] Fixes for pulicast v0.6.0
- [Minor] New default port 8765

## [v2.2.2](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.2.1...v2.2.2) - 15.09.20
### Changed
- [Patch] Avoid crash when resized too small
- [Patch] Added hostname to pulicast node name

## [v2.2.1](https://gitlab.com/kiteswarms/puli-sniffer/compare/v2.2.0...v2.2.1) - 24.08.20
### Added
- [patch] windows curses dependency when on nt system

### Changed
- [Patch] Updated getFieldDataTypes exception message

## [v2.2.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/2.1.0...v2.2.0) - 17.07.20
### Added
- [Minor] First pip release

## [2.1.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/2.0.0...2.1.0) - 17.07.20
### Added
- [Minor] Added node information view to puli-sniffer

## [2.0.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.4.0...2.0.0) - 19.06.20
### Changed
- [Major] Changed message transport from zerocm to pulicast
### Fixed
- [Patch] Fixed crashing due to window resize

## [1.4.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.3.4...1.4.0) - 08.05.20
### Added
- [Minor] Added support for nested message types of depth 1

## [1.3.4](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.3.3...1.3.4) - 28.05.20
### Added
- [Patch] Added support for boolean message type

## [1.3.3](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.3.2...1.3.3) - 28.02.20
### Fixed
- [Patch] Fixed frequency computation
- [Patch] Channel ordering case insensitive
- [Patch] Updated user interface

## [1.3.2](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.3.1...1.3.2) - 21.02.20
### Added
- [Patch] Added support for new lines

## [1.3.1](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.3.0...1.3.1) - 12.02.20
### Added
- [Patch] Added frequency computation and stability check

## [1.3.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.2.1...1.3.0) - 12.02.20
### Added
- [Minor] Added pause feature to detail view
- [Patch] Added timestamp to detail view
### Changed
- [Patch] Changed frequency result to median over time window
- [Patch] Allow navigation with arrow keys only

## [1.2.1](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.2.0...1.2.1) - 23.01.20
### Fixed
- [Patch] Fixed unresolved field type detection

## [1.2.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.1.0...1.2.0) - 22.01.20
### Added
- [Minor] Extraction of datatypes from ZCM classes
### Fixed
- [Patch] Fixed clearing of chrono view

## [1.1.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/1.0.0...1.1.0) - 20.01.20
### Added
- [Minor] Added hotkey for list clearing

## [1.0.0](https://gitlab.com/kiteswarms/puli-sniffer/compare/0.1.0...1.0.0) - 20.01.20
### Added
- [Major] Added status bar

## [0.1.0](https://gitlab.com/kiteswarms/puli-sniffer/-/tree/0.1.0) - 17.01.20
### Added
- [Minor] Added puli-sniffer
- [Minor] Added puli-log-sniffer
