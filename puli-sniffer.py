#!/usr/bin/python3
import sys

from puli_sniffer.sniffer_main import main


if __name__ == "__main__":
    sys.exit(main())
