#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import pulicast
from pulicast.discovery.node_discoverer import NodeDiscoverer
from pulicast.discovery.node_view import NodeView
from pulicast.discovery.channel_discoverer import ChannelDiscoverer
from pulicast.discovery.channel_view import ChannelView
import os
import argparse
import curses
import string
import time
import datetime
import zcom
import socket

from . import sniffer
from .common import SnifferDisplayPage
from .utils import zcm_utils
from .sniffer.common import getPrecTime

DEFAULT_PORT = 8765

LINE_WIDTH  = 100
HEADER_SIZE = 4
FOOTER_SIZE = 1

HEADER_TILE_TEXT = "PULI-STATE-SNIFFER"

HISTORYLENGTH = 100

COLOR_DEFAULT             = sniffer.registerColor(curses.COLOR_WHITE, curses.COLOR_BLACK)
COLOR_SELECTED            = sniffer.registerColor(230, 238)  # pale yellow on dark gray
COLOR_HEADER              = sniffer.registerColor(113, 237)  # grass green on dark gray
COLOR_HEADER2             = sniffer.registerColor(121, 237)  # pale green on dark gray
COLOR_HEADER3             = sniffer.registerColor(185, 237)  # rose pink on dark gray
COLOR_HEADER_FILTERED     = sniffer.registerColor(214, 237)  # yellow on dark gray
COLOR_HEADER_ALT          = sniffer.registerColor(246, 237)  # light gray on dark gray
COLOR_HEADER_BAR          = sniffer.registerColor(232,  113)  # black on grass green
COLOR_HEADER_BAR2         = sniffer.registerColor(232,  121)  # black on dim blue
COLOR_HEADER_BAR3         = sniffer.registerColor(232, 185)  # black on rose pink
COLOR_HEADER_BAR_FILTERED = sniffer.registerColor(232, 214)  # black on yellow
COLOR_MARKED              = sniffer.registerColor(251, 124)  # light gray on dark red
COLOR_MARKED_SELECTED     = sniffer.registerColor(230, 160)   # black on red
COLOR_FILTER_ERROR        = sniffer.registerColor(196, 237)  # red on dark gray

FILTER_CHARACTERS = string.ascii_letters + string.digits + "!~#_- =/.^$*+?{}[]\\|()"

# Allocate the curses windows globally (curses sometimes hard crashes otherwise)
WINDOW_HEADER = None
WINDOW_MAIN   = None
WINDOW_FOOTER = None

# Global list for general log message history (chrono view)
g_cmdMessageHistory = []
g_cmdMessageHistory_updated = False


def clearGlobalHistory():
    global g_cmdMessageHistory, g_cmdMessageHistory_updated
    g_cmdMessageHistory = []
    g_cmdMessageHistory_updated = True


# ------------------------------------------------------------------------------
class SnifferOriginListDisplay(SnifferDisplayPage):
    COLUMN_WIDTH_MAX = (6, 20, 20, 10, 14, 24)
    COLUMN_ALIGNMENT = (1, -1, -1, 1, 1, -1)
    COLUMN_AUTOSIZE  = (0, 1, 1, 0, 0, 1)
    selected_idx = 0
    _scroll_offset = 0
    _column_size = COLUMN_WIDTH_MAX
    _pressed_index = -1

    color_header_title     = COLOR_HEADER
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def ensureVisible(self, idx):
        winH, _ = WINDOW_MAIN.getmaxyx()
        start = self._scroll_offset
        if len(self.channels) <= winH:
            self._scroll_offset = 0
        elif (self._scroll_offset + winH) > len(self.channels):
            self._scroll_offset = len(self.channels) - winH
        elif idx <= start:
            self._scroll_offset = max(0, idx - 1)
        elif idx >= (start + winH - 1):
            self._scroll_offset = min(idx - winH + 2, len(self.channels) - winH)
        else:
            return False
        return True

    def getMinColumnWidth(self, max_lines=1):
        winH, winW = WINDOW_MAIN.getmaxyx()
        min_colw = (0,) * len(self.COLUMN_WIDTH_MAX)
        # Draw the channel values line by line
        for info in self.channels.values():
            if info.index < self._scroll_offset or info.index >= (self._scroll_offset + max_lines):
                continue
            last_msg  = info.last_messages[-1][1].state
            last_dest = info.last_messages[-1][1].destination
            chn_sizes = (1, len(info.name) + 1, len(last_dest) + 1, 1, 1, len(last_msg) + 1)
            # Update max content size values
            min_colw = tuple(max(i) for i in zip(chn_sizes, min_colw))
        return min_colw

    def computeColumnSizes(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        fixed  = sum(w for w, f in zip(self.COLUMN_WIDTH_MAX, self.COLUMN_AUTOSIZE) if not f)
        remain = float(winW - 2 - fixed)
        sizes  = list(self.COLUMN_WIDTH_MAX)
        min_colw = self.getMinColumnWidth(winH)
        for col, frac in enumerate(self.COLUMN_AUTOSIZE):
            if not frac:
                continue
            calc = int(remain * frac / sum(self.COLUMN_AUTOSIZE))
            # sizes[col] = min(sizes[col], max(min_colw[col], calc))
            sizes[col] = min(max(min_colw[col], sizes[col]), calc)
        old_col_size = self._column_size
        self._column_size = tuple(sizes)
        return old_col_size != self._column_size

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)

    def reset(self):
        # Compute column sizes
        self.computeColumnSizes()
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.ensureVisible(self.selected_idx)
        self.repaintMessageList()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def repaintHeader(self, bar_only=False):
        title_text = " - ".join((HEADER_TILE_TEXT, "Origin View"))
        # Construct header bar text
        header_bar = ""
        col_titles = ("", "Origin", "Destination", "Counter", "Freq (Hz)  ", "State")
        for col, text in enumerate(col_titles):
            if self.COLUMN_ALIGNMENT[col] == 1:
                header_bar += text.rjust(self._column_size[col], ' ')
            else:
                header_bar += text.ljust(self._column_size[col], ' ')
        header_end = self._getListIndent() + len(header_bar)
        header_bar = header_bar.rjust(header_end, ' ')
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, bar_only=bar_only)

    def repaintMessageList(self, index_list=(), changed_only=False):
        winH, winW = WINDOW_MAIN.getmaxyx()
        maxW = winW - 1
        indent = self._getListIndent()
        # Draw the channel values line by line
        for idx, info in enumerate(self.channels.values()):
            if idx < self._scroll_offset or idx >= (self._scroll_offset + winH):
                continue
            if index_list and idx not in index_list:
                continue
            if changed_only and not info.changed():
                continue
            info.touch()
            # Collect the data
            last_msg  = info.last_messages[-1][1].state
            last_dest = info.last_messages[-1][1].destination
            freq_text = "{:.1f} ".format(info.frequency)
            cell_data = (str(idx + 1) + ": ", info.name, last_dest, str(info.counter), freq_text,
                         last_msg)
            # Construct the channel row
            channel_row = ""
            for col, text in enumerate(cell_data):
                colspan = self._column_size[col]
                if (col == 4):
                    freqCol = len(channel_row)
                    freqText = text.rjust(colspan - 2, ' ')
                if self.COLUMN_ALIGNMENT[col] == 1:
                    channel_row += text.rjust(colspan, ' ')[:colspan]
                else:
                    channel_row += text.ljust(colspan, ' ')[:colspan]
            channel_row = channel_row.ljust(LINE_WIDTH, ' ')
            xPos  = indent
            yPos  = idx - self._scroll_offset
            color1 = None
            color2 = None

            if (idx == self.selected_idx):
                color1 = COLOR_SELECTED
                color2 = COLOR_SELECTED
            if (info.flagged_reason):
                color2 = COLOR_MARKED
                if (idx == self.selected_idx):
                    color2 = COLOR_MARKED_SELECTED

            if self.addstr(WINDOW_MAIN, yPos, xPos, channel_row[:maxW], col=color1):
                WINDOW_MAIN.touchline(yPos, 1, True)
            self.addstr(WINDOW_MAIN, yPos, freqCol + 1 + xPos, freqText, col=color2)
        # Repaint the window
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Switch to Chrono View (Tab)   Enter detail view (Return)   " \
            "Clear filter (Escape)   Clear list (ctrl + r)"
        if not self.channels.filter:
            footer_color = self.color_header_title
        else:
            footer_color = self.color_header_title_alt
        WINDOW_FOOTER.bkgd(curses.color_pair(footer_color))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def setSelection(self, idx, ensure_visible=True):
        if self.selected_idx == idx:
            return
        old_idx = self.selected_idx
        self.selected_idx = idx
        index_list = None
        if not ensure_visible:
            index_list = (old_idx, idx)
        elif not self.ensureVisible(idx):
            index_list = (old_idx, idx)
        self.repaintMessageList(index_list=index_list)

    def _getListIndent(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        return max(0, int((winW - sum(self._column_size) - 1) / 2))

    def setDisplayFilter(self, filter_str):
        # Find the channel name for the selected channel
        try:
            channel_name = self.channels.keys()[self.selected_idx]
        except BaseException:
            channel_name = None
        self.channels.setFilter(filter_str)
        # Reselect the previously selected channel if not filtered out
        idx = self.channels.index(channel_name)
        if idx >= 0:
            self.selected_idx = idx
        else:
            self.selected_idx = max(0, min(self.selected_idx, len(self.channels) - 1))
        self.ensureVisible(self.selected_idx)
        # Update the display
        self.reset()

    def update(self):
        # Update the header and list if new channels were added
        if self.core.channels_updated:
            self.reset()
        # Repaint the entire list if the frequencies are updated
        elif self.core.frequency_updated:
            self.repaintMessageList()
        else:
            # Update the counter display of channels with new value
            self.repaintMessageList(changed_only=True)

    def handleInput(self, key):
        # 'enter' or 'right-arrow' to switch to detail view
        if key in ('\n', "PADENTER", "KEY_RIGHT", "KEY_B3"):
            # Find the channel info structure for the selected channel
            try:
                channel_name = self.channels.keys()[self.selected_idx]
                detailPage = self.parent.getPage("state_channel_detail")
                if detailPage and detailPage.setChannel(channel_name):
                    self.parent.showPage("state_channel_detail")
            except BaseException:
                pass
        elif (key == '\t'):
            self.parent.showPage("command_channel_chrono")
        elif key in ("KEY_DOWN", "KEY_C2"):
            # Select the next item down
            new_idx = min(len(self.channels) - 1, self.selected_idx + 1)
            self.setSelection(new_idx)
        elif key in ("KEY_UP", "KEY_A2"):
            # Select the next item up
            new_idx = max(0, self.selected_idx - 1)
            self.setSelection(new_idx)
        elif key in ("KEY_NPAGE", "KEY_C3"):
            # Select the next item half the visible list down
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = min(len(self.channels) - 1, self.selected_idx + int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_PPAGE", "KEY_A3"):
            # Select the next item half the visible list up
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = max(0, self.selected_idx - int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_HOME", "KEY_A1"):
            self.setSelection(0)
        elif key in ("KEY_END", "KEY_C1"):
            self.setSelection(len(self.channels) - 1)
        elif key in FILTER_CHARACTERS:
            filter_str = key
            if self.channels.filter:
                filter_str = self.channels.filter + key
            self.setDisplayFilter(filter_str)
        elif key in ("KEY_BACKSPACE", '\x08'):  # 'backspace' to remove last char in filter
            filter_str = ""
            if self.channels.filter:
                filter_str = self.channels.filter[:-1]
            self.setDisplayFilter(filter_str)
        elif key in ('\x1b',):  # 'escape' to clear the filter
            if self.channels.filter:
                self.setDisplayFilter("")
        elif key in ('\x12',):  # 'ctrl + r' results in display reset
            self.selected_idx = 0
            clearGlobalHistory()
            self.core.reset()
        elif key in ("KEY_MOUSE",):
            try:
                _, x, y, _, bstate = curses.getmouse()
                top, left = WINDOW_MAIN.getbegyx()
                winH, winW = WINDOW_MAIN.getmaxyx()
                maxY = top + min(len(self.channels), winH)
                if x < (left + LINE_WIDTH) and y >= top and y < maxY:
                    new_idx = y - top + self._scroll_offset
                    if bstate & curses.BUTTON1_PRESSED:
                        self.setSelection(new_idx, ensure_visible=False)
                        self._pressed_index = new_idx
                    if bstate & curses.BUTTON1_RELEASED:
                        if new_idx == self.selected_idx or self._pressed_index is None:
                            self.setSelection(new_idx, ensure_visible=False)
                            self.showDetailView()
                        self._pressed_index = None
            except BaseException:
                pass
        else:
            # open("debug.txt", "a").write(repr(key) + "\n")
            pass


# ------------------------------------------------------------------------------
class SnifferOriginDetailDisplay(SnifferDisplayPage):
    COLUMN_WIDTH_MAX = (14, 20, 20, 34)
    COLUMN_ALIGNMENT = (-1, -1, 1, -1)
    COLUMN_AUTOSIZE  = (0, 1, 0, 1)
    selected_idx = 0
    _scroll_offset = 0
    _column_size = COLUMN_WIDTH_MAX
    _pressed_index = -1

    color_header_title     = COLOR_HEADER2
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR2
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def setChannel(self, channel_name):
        # Find the channel info structure for the selected channel
        self.detail_channel = self.channels.get(channel_name)
        if self.detail_channel is None:
            return False
        return True

    def getMinColumnWidth(self, max_lines=1):
        winH, winW = WINDOW_MAIN.getmaxyx()
        min_colw = (0,) * len(self.COLUMN_WIDTH_MAX)
        # Draw the channel values line by line
        for idx, (timestamp, msg) in enumerate(reversed(self.detail_channel.last_messages)):
            if idx >= max_lines:
                break
            chn_sizes  = (1, len(msg.destination) + 1, 1, len(msg.state) + 1)
            # Update max content size values
            min_colw = tuple(max(i) for i in zip(chn_sizes, min_colw))
        return min_colw

    def computeColumnSizes(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        fixed  = sum(w for w, f in zip(self.COLUMN_WIDTH_MAX, self.COLUMN_AUTOSIZE) if not f)
        remain = float(winW - 2 - fixed)
        sizes  = list(self.COLUMN_WIDTH_MAX)
        min_colw = self.getMinColumnWidth(winH)
        for col, frac in enumerate(self.COLUMN_AUTOSIZE):
            if not frac:
                continue
            calc = int(remain * frac / sum(self.COLUMN_AUTOSIZE))
            # sizes[col] = min(sizes[col], max(min_colw[col], calc))
            sizes[col] = min(max(min_colw[col], sizes[col]), calc)
        old_col_size = self._column_size
        self._column_size = tuple(sizes)
        return old_col_size != self._column_size

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)

    def reset(self):
        # Compute column sizes
        self.computeColumnSizes()
        self.repaintHeader()
        WINDOW_MAIN.erase()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def repaintHeader(self, bar_only=False):
        title_text = " - ".join((self.detail_channel.name, HEADER_TILE_TEXT, "Detail View"))
        # Construct header bar text
        header_bar = ""
        for col, text in enumerate(("Local Time", "Destination", "Timestamp   ", "State")):
            if self.COLUMN_ALIGNMENT[col] == 1:
                header_bar += text.rjust(self._column_size[col], ' ')
            else:
                header_bar += text.ljust(self._column_size[col], ' ')
        header_end = self._getListIndent() + len(header_bar)
        header_bar = header_bar.rjust(header_end, ' ')
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, show_filter=False,
                            bar_only=bar_only)

    def repaintMessageList(self, index_list=(), changed_only=False):
        winH, winW = WINDOW_MAIN.getmaxyx()
        WINDOW_MAIN.clear()
        linesToPaint = min(winH, len(self.detail_channel.last_messages))

        indent = self._getListIndent()
        line = 0
        for timestamp, data in reversed(self.detail_channel.last_messages[-linesToPaint:]):
            tstext_local = datetime.datetime.fromtimestamp(timestamp).strftime('%H:%M:%S')
            tstext_msg = f"{data.ts * 1e-9:.1f}   "

            cell_data = (tstext_local, data.destination, tstext_msg, data.state)

            xPos  = indent
            yPos  = line - self._scroll_offset
            for col, text in enumerate(cell_data):
                colspan = self._column_size[col]
                if self.COLUMN_ALIGNMENT[col] == 1:
                    cell_text = text.rjust(colspan, ' ')[:colspan]
                else:
                    cell_text = text.ljust(colspan, ' ')[:colspan]

                self.addstr(WINDOW_MAIN, yPos, xPos, cell_text)
                xPos += len(cell_text)
            line += 1
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Back to Origin View (Backspace)"
        WINDOW_FOOTER.bkgd(curses.color_pair(self.color_header_title))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def _getListIndent(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        return max(0, int((winW - sum(self._column_size) - 1) / 2))

    def update(self):
        # Update the header and list if new channels were added
        if self.core.channels_updated:
            self.reset()
        # Repaint the entire list if the frequencies are updated
        elif self.core.frequency_updated:
            self.repaintMessageList()
        else:
            # Update the counter display of channels with new value
            self.repaintMessageList(changed_only=True)

    def handleInput(self, key):
        # 'backspace', 'escape', 'left-arrow' to go back
        if key in ("KEY_BACKSPACE", '\x08', '\x1b', "KEY_LEFT", "KEY_B1"):
            self.parent.showPage("state_channel_origin")


# ------------------------------------------------------------------------------
class SnifferCommandListDisplay(SnifferDisplayPage):
    COLUMN_WIDTH_MAX = (14, 20, 20, 20, 32)
    COLUMN_ALIGNMENT = (-1, -1, -1, 1, -1)
    COLUMN_AUTOSIZE  = (0, 1, 1, 0, 1)
    selected_idx = 0
    _scroll_offset = 0
    _column_size = COLUMN_WIDTH_MAX
    _pressed_index = -1

    color_header_title     = COLOR_HEADER3
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR3
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def setChannel(self, channel_name):
        # Find the channel info structure for the selected channel
        self.detail_channel = self.channels.get(channel_name)
        if self.detail_channel is None:
            return False
        return True

    def getMinColumnWidth(self, max_lines=1):
        min_colw = (0,) * len(self.COLUMN_WIDTH_MAX)
        # Draw the channel values line by line
        for idx, (data, timestamp) in enumerate(reversed(g_cmdMessageHistory)):
            if idx >= max_lines:
                break
            chn_sizes = (1, len(data.origin) + 1, len(data.destination) + 1, 1,
                         len(data.command) + 1)
            # Update max content size values
            min_colw = tuple(max(i) for i in zip(chn_sizes, min_colw))
        return min_colw

    def computeColumnSizes(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        fixed  = sum(w for w, f in zip(self.COLUMN_WIDTH_MAX, self.COLUMN_AUTOSIZE) if not f)
        remain = float(winW - 2 - fixed)
        sizes  = list(self.COLUMN_WIDTH_MAX)
        min_colw = self.getMinColumnWidth(winH)
        for col, frac in enumerate(self.COLUMN_AUTOSIZE):
            if not frac:
                continue
            calc = int(remain * frac / sum(self.COLUMN_AUTOSIZE))
            # sizes[col] = min(sizes[col], max(min_colw[col], calc))
            sizes[col] = min(max(min_colw[col], sizes[col]), calc)
        old_col_size = self._column_size
        self._column_size = tuple(sizes)
        return old_col_size != self._column_size

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)

    def reset(self):
        # Compute column sizes
        self.computeColumnSizes()
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.repaintMessageList()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def repaintHeader(self, bar_only=False):
        title_text = " - ".join((HEADER_TILE_TEXT, "Chronological Command View"))
        # Construct header bar text
        header_bar = ""
        col_titles = ("Local Time", "Origin", "Destination", "Timestamp   ", "Command")
        for col, text in enumerate(col_titles):
            if self.COLUMN_ALIGNMENT[col] == 1:
                header_bar += text.rjust(self._column_size[col], ' ')
            else:
                header_bar += text.ljust(self._column_size[col], ' ')
        header_end = self._getListIndent() + len(header_bar)
        header_bar = header_bar.rjust(header_end, ' ')
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, show_filter=False,
                            bar_only=bar_only)

    def repaintMessageList(self, index_list=(), changed_only=False):
        winH, winW = WINDOW_MAIN.getmaxyx()
        # WINDOW_MAIN.clear()
        WINDOW_MAIN.touchwin()
        linesToPaint = min(winH, len(g_cmdMessageHistory))

        indent = self._getListIndent()
        line = 0
        for data, timestamp in reversed(g_cmdMessageHistory[-linesToPaint:]):
            tstext_local = datetime.datetime.fromtimestamp(timestamp).strftime('%H:%M:%S')
            tstext_msg = f"{data.ts * 1e-9:.1f}   "
            cell_data = (tstext_local, data.origin, data.destination, tstext_msg, data.command)
            # Construct the channel row
            xPos  = indent
            yPos  = line - self._scroll_offset
            cell_text = ""
            for col, text in enumerate(cell_data):
                xPos += len(cell_text)
                colspan = self._column_size[col]
                if self.COLUMN_ALIGNMENT[col] == 1:
                    cell_text = text.rjust(colspan, ' ')[:colspan]
                else:
                    cell_text = text.ljust(colspan, ' ')[:colspan]

                self.addstr(WINDOW_MAIN, yPos, xPos, cell_text)
            line += 1
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Switch to Origin View (Tab)   Clear list (ctrl + r)"
        WINDOW_FOOTER.bkgd(curses.color_pair(self.color_header_title))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def _getListIndent(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        return max(0, int((winW - sum(self._column_size) - 1) / 2))

    def update(self):
        global g_cmdMessageHistory_updated
        # Update the header and list if new channels were added
        if g_cmdMessageHistory_updated:
            if self.computeColumnSizes():
                self.repaintHeader(bar_only=True)
            self.repaintMessageList()
        g_cmdMessageHistory_updated = False

    def handleInput(self, key):
        if key in ('\t',):  # 'tab' to go back
            self.parent.showPage("state_channel_origin")
        elif key in ('\x12',):  # 'ctrl + r' results in display reset
            self.selected_idx = 0
            clearGlobalHistory()
            self.core.reset()
            self.reset()


# ------------------------------------------------------------------------------
def main():
    global WINDOW_HEADER, WINDOW_MAIN, WINDOW_FOOTER, startTime
    # Parse the commandline arguments
    description = "Terminal based spy utility. Subscribes to all channels on a pulicast transport" \
                  " and displays them in an interactive terminal."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-p", "--port", default=pulicast.DEFAULT_PORT, type=int,
                        help="Pulicast capture port")
    parser.add_argument("-i", "--interface", default="0.0.0.0", type=str,
                        help="Pulicast capture interface ip address")
    parser.add_argument("-d", "--decoder-path", default=["zcom"], type=str, action='append',
                        help="Path to (or name of) modules/package with message decoder objects")
    args = parser.parse_args()

    # Construct connection info string
    con_info = "port: " + str(args.port) + " | interface: " + args.interface

    # Load message decoder
    for path in args.decoder_path:
        zcm_utils.loadMessageDecoder(path)

    # Create and start a pulicast socket node
    node_name = "{} ({})".format(HEADER_TILE_TEXT.lower(), socket.gethostname())
    puli = pulicast.ThreadedNode(node_name, port=args.port, interface_address=args.interface)
    # Start the background thread for package processing
    puli.start()

    def stateMessageHandler(msg: zcom.statemessage):
        state_channel_core.handleMessage(msg.origin, msg)

    def commandMessageHandler(msg: zcom.commandmessage):
        global g_cmdMessageHistory, g_cmdMessageHistory_updated
        if (len(g_cmdMessageHistory) > HISTORYLENGTH):
            g_cmdMessageHistory.pop(0)
        g_cmdMessageHistory.append((msg, time.time()))
        g_cmdMessageHistory_updated = True

        command_channel_core.handleMessage(msg.origin, msg)

    startTime = getPrecTime()
    # Create core and subscribe to all channels
    state_channel_core = sniffer.Core(history_length=HISTORYLENGTH)
    command_channel_core = sniffer.Core(history_length=HISTORYLENGTH)
    puli["state"].subscribe(stateMessageHandler)
    puli["command"].subscribe(commandMessageHandler)

    # Create the main app
    app = sniffer.App()
    # Create the curses windows for the views (to avoid curses crashes in some instances)
    scrH, scrW = app.stdscr.getmaxyx()
    WINDOW_HEADER = curses.newwin(HEADER_SIZE, scrW - 1, 0, 0)
    WINDOW_HEADER.bkgd(curses.color_pair(COLOR_HEADER))
    WINDOW_MAIN = curses.newwin(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1, HEADER_SIZE, 0)
    WINDOW_MAIN.bkgd(curses.color_pair(COLOR_DEFAULT))
    WINDOW_FOOTER = curses.newwin(FOOTER_SIZE, scrW - 1, scrH - FOOTER_SIZE, 0)

    # Create the view objects
    app.addPage("state_channel_origin", SnifferOriginListDisplay(app, state_channel_core, con_info))
    app.addPage("state_channel_detail", SnifferOriginDetailDisplay(app, state_channel_core, con_info))
    app.addPage("command_channel_chrono", SnifferCommandListDisplay(app, command_channel_core, con_info))
    # Start main loop - wait for exit command on stdin
    app.loop()

    # Cleanup
    app.destroy()
    puli.stop()

    return 0
