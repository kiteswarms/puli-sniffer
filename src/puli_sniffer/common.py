#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
from .sniffer import DisplayPage


# ------------------------------------------------------------------------------
class SimpleQuoteStripper():
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return repr(self.value)[1:-1]


# ------------------------------------------------------------------------------
class SnifferDisplayPage(DisplayPage):
    color_header_title     = None  # Title text color (default)
    color_header_title_alt = None  # Title text color (with active filter)
    color_header_bar       = None  # Header bar color (default)
    color_header_bar_alt   = None  # Header bar color (with active filter)
    color_header_info      = None  # Header info text color (default)
    color_filter_error     = None  # Filter text color (invalid regex syntax)

    def _repaintHeader(self, win, title, bar_text, show_filter=True, force_alt_color=False,
                       bar_only=False):
        winH, winW = win.getmaxyx()
        maxW = winW - 1
        filter_str = self.channels.filter or ""
        if not (show_filter and filter_str) and not force_alt_color:
            title_color = self.color_header_title
            header_bar_color = self.color_header_bar
        else:
            title_color = self.color_header_title_alt
            header_bar_color = self.color_header_bar_alt

        if not bar_only:
            # Clear the window
            win.touchwin()
            win.erase()
            # Draw the channel filter top left corner
            if filter_str and show_filter:
                if self.channels.filter_valid:
                    filter_color = self.color_header_info
                else:
                    filter_color = self.color_filter_error
                filter_text  = "Filter (regex): '{}'".format(filter_str)
                self.addstr(win, 0, 2, filter_text[:maxW - 2], filter_color)
                num_filtered = self.channels.size(filtered=True)
                num_total    = self.channels.size(filtered=False)
                amount_text  = "Showing {} of {}".format(num_filtered, num_total)
                self.addstr(win, 2, 2, amount_text[:maxW - 2], self.color_header_info)
            # Draw the zcm url in the top right corner
            url_text = self.con_info[:maxW - 1]
            url_pos  = maxW - len(url_text) - 1
            self.addstr(win, 0, url_pos, url_text, self.color_header_info)
            # Draw the title in the center
            title_text  = title[:maxW]
            title_pos   = int((maxW - len(title_text)) / 2)
            self.addstr(win, 1, title_pos, title_text, title_color)
        # Draw the header bar
        header_bar = bar_text.ljust(maxW, ' ')
        self.addstr(win, 3, 0, header_bar[:maxW], header_bar_color)
        win.touchline(3, 1, True)
        # Repaint the window
        win.refresh()
