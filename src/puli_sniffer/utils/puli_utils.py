#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import time
import bisect

from pulicast.discovery.node_view import NodeView

from ..sniffer.common import getPrecTime, FilteredMap


UPDATE_FREQUENCY = 5


# ------------------------------------------------------------------------------
class NodeInfo:
    name = ""
    session_id = 0
    index = 0
    counter = 0
    timestamp = 0.0
    node = NodeView
    _updated = True

    def __init__(self, **kwargs):
        vars(self).update(kwargs)

    def updateNode(self, timestamp, node):
        self.counter += 1
        self.timestamp = timestamp
        self.node = node
        self._updated = True

    def incrementIndex(self):
        self.index += 1
        self._updated = True

    def decrementIndex(self):
        self.index -= 1
        self._updated = True

    def changed(self):
        return self._updated

    def touch(self):
        self._updated = False


# ------------------------------------------------------------------------------
class PuliNodeCore():
    _run = True
    channels_updated = False

    def __init__(self, history_length=1):
        self._reftime = time.time() - getPrecTime()
        self.channels = FilteredMap()

    def handleNode(self, node_key: str, node: NodeView):
        # Normalize timestamp to UTC time
        timestamp = getPrecTime() + self._reftime
        if node_key in self.channels:
            return

        # Add the new channel in the alphabetically correct position
        node_infos = self.channels.values(filtered=False)
        node_names = tuple(map(lambda info: info.name.lower(), node_infos))
        idx = bisect.bisect(node_names, node.name.lower())
        for info in node_infos[idx:]:
            info.incrementIndex()
        # Add the new channel
        node_info = NodeInfo(name=node.name, session_id=node.session_id, index=idx)
        node_info.updateNode(timestamp, node)
        self.channels[node_key] = node_info
        # Notify the display updater thread
        self.channels_updated = True

    def removeNode(self, node_key: str):
        if node_key in self.channels:
            node = self.channels.pop(node_key)
            node_infos = self.channels.values(filtered=False)
            for info in node_infos[node.index:]:
                info.decrementIndex()
            # Notify the display updater thread
            self.channels_updated = True

    def touch(self):
        self.channels_updated  = False

    def reset(self):
        self.channels.clear()
        self.channels_updated = True

    def stop(self):
        self._run = False
