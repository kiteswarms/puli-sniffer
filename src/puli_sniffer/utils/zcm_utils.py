#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import sys
import os
import dis
import inspect
import glob


# Maps the data type formatter from pythons 'struct' module to a primitive type in zerocm
ZCM_TYPE_NAME = {
    "b": "int8_t",
    "h": "int16_t",
    "i": "int32_t",
    "q": "int64_t",
    "f": "float",
    "d": "double",
}

ZCM_TYPE_CACHE = {}


# ------------------------------------------------------------------------------
class GenericDecoder():
    register = {}

    @classmethod
    def decode(self, data):
        # Check if the message prefix is known
        fingerprint = data[:8]
        if fingerprint in self.register:
            return data

    @classmethod
    def parseData(self, data):
        # Decode the data with the registered decoder
        fingerprint = bytes(data[:8])
        if fingerprint in self.register:
            return self.register[fingerprint].decode(data)

    @classmethod
    def getMessageType(self, data):
        # Decode the data with the registered decoder
        fingerprint = bytes(data[:8])
        return self.register.get(fingerprint)


# ------------------------------------------------------------------------------
def loadMessageDecoder(name):
    path    = None
    names   = set()
    # Find single modules, packages and modules in directories
    if os.path.exists(name):
        abspath = os.path.abspath(name)
        path, name = os.path.split(abspath)
        if os.path.isdir(abspath):
            if glob.glob(os.path.join(abspath, "__init__.py*")):
                names.add(name)
            else:
                for name in glob.glob(os.path.join(abspath, "*.py*")):
                    names.add(os.path.basename(name).rsplit('.', 1)[0])
                path = abspath
        else:
            names.add(name.rsplit('.', 1)[0])
    else:
        names.add(name)
    # Load all collected modules by name
    path and sys.path.insert(0, path)
    modules = []
    module = None
    for name in names:
        try:
            module = __import__(name)
            modules.append(module)
        except ImportError:
            pass
    path and sys.path.pop(0)
    # If a path was found but failed to import, try again without the path in the sys.path
    if path and not module:
        for name in names:
            try:
                module = __import__(name)
                modules.append(module)
            except ImportError:
                pass
    # Find objects in modules that have the interface for ZCM message decoding
    for module in modules:
        for attr in dir(module):
            try:
                obj = getattr(module, attr)
                if not hasattr(obj, "__slots__") or not hasattr(obj, "_get_packed_fingerprint"):
                    continue
                if not hasattr(obj, "decode") or not hasattr(obj, "encode"):
                    continue
                fingerprint = obj._get_packed_fingerprint()
                # Add the decoder object to the register in the decoder wrapper object
                GenericDecoder.register[fingerprint] = obj
                # decoder_map[obj] = (obj.__name__, obj.__slots__)
            except BaseException:
                continue


# ------------------------------------------------------------------------------
def getFieldDataTypes(zcm_type):
    """
    Analyses the code of a method in zcm message classes to determine the zcm specific
    data types.
        Result: Dictionary mapping field names to a type string

    Note: fields of which the type is not known are left out
    """
    # Return cached field type map
    if zcm_type in ZCM_TYPE_CACHE:
        return ZCM_TYPE_CACHE[zcm_type]
    # Ensure the given argument is an actual zcm decoder
    if not hasattr(zcm_type, "__slots__") or not hasattr(zcm_type, "_decode_one"):
        raise TypeError("expected zcm message type class, got " + repr(zcm_type))
    type_map = {}
    module = inspect.getmodule(zcm_type)
    fields = zcm_type.__slots__
    # Group the instructions of the '_decode_one' method by lines
    line_instruct = []
    for inst in dis.get_instructions(zcm_type._decode_one.__code__):
        if inst.starts_line is not None:
            line_instruct.append([inst])
        else:
            line_instruct[-1].append(inst)
    self_arg = list(l for l in line_instruct[0] if l.opname == "STORE_FAST")[0].argval
    # Walk through the instructions grouped by lines
    line_num = 0
    while line_num < len(line_instruct):
        linst = line_instruct[line_num]
        line_num += 1
        # Determine the data types if the 'struct.unpack' result is directly assigned to fields
        if ( linst[0].opname == "LOAD_GLOBAL" and linst[0].argval == "struct" and
             linst[1].opname.startswith("LOAD_") and linst[1].argval == "unpack"):
            type_format = linst[2].argval.strip("><!=")
            unpack_fields = []
            for i in range(3, len(linst))[::-2]:
                if linst[i].opname != "STORE_ATTR":
                    break
                if linst[i - 1].opname != "LOAD_FAST" or linst[i - 1].argval != self_arg:
                    break
                unpack_fields.insert(0, linst[i].argval)
            # The fields have single value and are assigned together
            if len(type_format) == len(unpack_fields):
                type_map.update(zip(unpack_fields, map(ZCM_TYPE_NAME.get, type_format)))
            # One single field is assigned with an array of one type
            if len(unpack_fields) == 1 and ("%" in type_format or type_format[:-1].isdigit()):
                type_map[unpack_fields[0]] = ZCM_TYPE_NAME.get(type_format[-1])
        # Determine the data is decoded as single bool value
        elif (linst[0].opname == "LOAD_GLOBAL" and linst[0].argval == "bool" and
              linst[1].opname == "LOAD_GLOBAL" and linst[1].argval == "struct" and
              linst[2].opname.startswith("LOAD_") and linst[2].argval == "unpack"):
            if linst[-1].opname != "STORE_ATTR":
                continue
            if linst[-2].opname != "LOAD_FAST" or linst[-2].argval != self_arg:
                continue
            type_map[linst[-1].argval] = "boolean"
        # Determine if the data is directly assigned to the field (bytearray)
        elif (linst[0].opname == "LOAD_FAST" and linst[0].argval == "buf" and
              linst[1].opname.startswith("LOAD_") and linst[1].argval == "read"):
            if linst[-2].opname != "LOAD_FAST" or linst[-2].argval != self_arg:
                continue
            does_decode = False
            for idx in reversed(range(3, len(linst) - 2)):
                if linst[idx].opname.startswith("LOAD_") and linst[idx].argval == "decode":
                    if linst[idx + 1].opname == "LOAD_CONST" and linst[idx + 1].argval == "utf-8":
                        does_decode = True
                        break
            if linst[-1].opname == "STORE_ATTR":
                type_map[linst[-1].argval] = "string" if does_decode else "byte"
        # Determine if the field is a list of message classes
        elif (linst[0].opname == "SETUP_LOOP" and line_num < len(line_instruct)):
            linst = line_instruct[line_num]
            line_num += 1
            if len(linst) < 6:
                continue
            if not (linst[0].opname == "LOAD_FAST" and linst[0].argval == self_arg and
                    linst[2].opname.startswith("LOAD_") and linst[2].argval == "append" and
                    linst[4].opname.startswith("LOAD_") and linst[4].argval == "_decode_one"):
                continue
            sub_type = getattr(module, linst[3].argval)
            type_map[linst[1].argval] = sub_type.__module__
        # Determine if the field is a single message classes
        elif (linst[0].opname == "LOAD_GLOBAL" and
              linst[1].opname.startswith("LOAD_") and linst[1].argval == "_decode_one"):
            if linst[-2].opname != "LOAD_FAST" or linst[-2].argval != self_arg:
                continue
            if linst[-1].opname == "STORE_ATTR":
                sub_type = getattr(module, linst[0].argval)
                type_map[linst[-1].argval] = sub_type.__module__
    ref = zcm_type()
    for field in fields:
        if field not in type_map:
            attr = getattr(ref, field)
            if type(attr) is str:
                # Not captured by the rules above
                type_map[field] = "string"
            else:
                # Unable to determine type (can be another structure)
                type_map[field] = None
    # Store the field types in the cache
    ZCM_TYPE_CACHE[zcm_type] = type_map
    return type_map
