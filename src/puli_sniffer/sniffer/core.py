#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import time
import bisect

from .common import startThread, getPrecTime, ChannelInfo, FilteredMap


FLAG_FREQUENCY_DROP = "Channel package frequency suddenly dropped to zero"


UPDATE_FREQUENCY = 5  # frequency computations per second (Hz)
FILTER_WINDOW    = 20  # number of frequency computation intervals used for filtering
# Results in the total filter time window of 'FILTER_WINDOW / UPDATE_FREQUENCY'
MEDIAN_MIN_SAMPLES = 5


# ------------------------------------------------------------------------------
class Core():
    _run = True
    channels_updated = False
    frequency_updated = False

    def __init__(self, history_length=1):
        self._reftime = time.time() - getPrecTime()
        self.channels = FilteredMap()
        self.history_length = history_length
        # Start background thread
        startThread(self.__frequencyLoop)

    def handleMessage(self, channel, message):
        # Normalize timestamp to UTC time
        timestamp = getPrecTime() + self._reftime
        channel_info = self.channels.get(channel)
        if channel_info is not None:
            channel_info.setMessage(timestamp, message)
            if len(channel_info.last_messages) > self.history_length:
                channel_info.last_messages.pop(0)
            return
        # Add the new channel in the alphabetically correct position
        channel_names = self.channels.keys(filtered=False)
        idx = bisect.bisect(tuple(map(str.lower, channel_names)), channel.lower())
        for channel2 in channel_names[idx:]:
            self.channels[channel2].incrementIndex()
        # Add the new channel
        channel_info = ChannelInfo(name=channel, index=idx)
        channel_info.setMessage(timestamp, message)
        self.channels[channel] = channel_info
        # Notify the display updater thread
        self.channels_updated = True

    def touch(self):
        self.channels_updated  = False
        self.frequency_updated = False

    def reset(self):
        self.channels.clear()
        self.channels_updated = True

    def __frequencyLoop(self):
        def mean(e):
            return sum(e) / max(len(e), 1)

        def median(e):
            if not e:
                return 0
            return sorted(e)[int(len(e) / 2)]

        ltime = getPrecTime()
        last_diff_frame = {}
        diff_time_store = {}
        base_delay = delay = 1.0 / UPDATE_FREQUENCY
        while self._run:
            ctime = getPrecTime()
            for channel, info in list(self.channels.items()):
                # Get the mean of times between messages for previous time windows
                filtered_diffs = last_diff_frame.get(channel, [])
                last_diff_frame[channel] = filtered_diffs
                if len(filtered_diffs) >= FILTER_WINDOW:
                    filtered_diffs.pop(0)
                diff_times = diff_time_store.get(channel, []) + info.diff_times
                if len(diff_times) >= 1:
                    # Add the current mean message time gap
                    diffs = (min(diff_times), mean(diff_times), max(diff_times))
                    filtered_diffs.append(diffs)
                    if channel in diff_time_store:
                        diff_time_store.pop(channel)
                    # Clear the reason if the channel was flagged previously
                    info.flagged_reason = None
                else:
                    # Insufficient number of samples
                    filtered_diffs.append((0, 0, 0))
                    if diff_times:
                        diff_time_store[channel] = diff_times
                    elif channel in diff_time_store:
                        # Clear the chache if no data was received during the complete time window
                        if len(filtered_diffs) >= FILTER_WINDOW and sum(filtered_diffs) == 0:
                            diff_time_store.pop(channel)
                info.diff_times = []
                # Split the times into min, nominal and max diff times
                fmin, fnominal, fmax = tuple(zip(*filtered_diffs))
                # Select the most relevant time gap value and compute the frequency
                fnominal_select = list(diff for diff in fnominal if diff)
                # mean_time = fnominal_select and fnominal_select[-1] or 0.0
                mean_time = median(fnominal_select[-MEDIAN_MIN_SAMPLES:])
                frequency = mean_time and 1.0 / mean_time or 0.0
                # Pull the frequency value to '0' if the there is a significant drop
                compare_length = max(mean_time * UPDATE_FREQUENCY, MEDIAN_MIN_SAMPLES) * 1.5
                if sum(fnominal[-int(compare_length + 0.5):]) == 0:
                    frequency = 0
                    # Mark channels that have had somewhat regular traffic
                    #  - Minimal amount of readings required
                    #  - Stable frequency reached with less than 50% variance
                    if len(fnominal_select) > min(max(frequency / 2, 2), FILTER_WINDOW / 2):
                        time_var = max(fnominal_select) - min(fnominal_select)
                        if not time_var or (time_var / mean_time) < 0.5:
                            info.flagged_reason = FLAG_FREQUENCY_DROP
                info.frequency = frequency
            # Notify the display updater thread
            self.frequency_updated = True
            # Apply sleep delay adjustments to get as close to 1Hz as possible
            delay = max(delay + (base_delay - ctime + ltime) / 2, 0.0)
            ltime = ctime
            delay and time.sleep(delay)

    def stop(self):
        self._run = False
