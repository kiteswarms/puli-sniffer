#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import sys
import time
import bisect
import threading
import re


# ------------------------------------------------------------------------------
# Get the best possible precision for time differences
if sys.version_info[0] == 2 and sys.modules.get("nt"):
    # getPrecTime = time.clock  # Causes high CPU load
    getPrecTime = time.time
elif sys.version_info[:2] >= (3, 3):
    getPrecTime = time.perf_counter
else:
    getPrecTime = time.time


# ------------------------------------------------------------------------------
def startThread(target, *args, **kwargs):
    thr = threading.Thread(target=target, args=args, kwargs=kwargs)
    thr.daemon = True
    thr.start()
    return thr


# ------------------------------------------------------------------------------
class ChannelInfo:
    name = ""
    index = 0
    counter = 0
    frequency = 0.0
    last_messages = []
    diff_times = []
    _updated = True
    flagged_reason = None  # Reason (str) for the channel being flagged (eg. frequency drop)

    def __init__(self, **kwargs):
        self.last_messages = []
        self.diff_times = []
        vars(self).update(kwargs)

    def setMessage(self, timestamp, message):
        self.counter += 1
        last_timestamp = self.last_messages and self.last_messages[-1][0] or 0.0
        self.diff_times.append(timestamp - last_timestamp)
        self.last_messages.append((timestamp, message))
        self._updated = True

    def incrementIndex(self):
        self.index += 1
        self._updated = True

    def changed(self):
        return self._updated

    def touch(self):
        self._updated = False


# ------------------------------------------------------------------------------
class FilteredMap(object):
    def __init__(self, sort_keys=True):
        self._dict  = {}
        self._keys  = []
        self._lock  = threading.Lock()
        self._sort  = sort_keys
        self.filter = ""
        self.filter_valid = True
        self._regex = None

    def setFilter(self, filter_str):
        self.filter = filter_str
        try:
            regex = None
            if filter_str:
                regex = re.compile(filter_str, re.IGNORECASE)
        except re.error:
            self.filter_valid = False
            return False
        self.filter_valid = True
        self._regex = regex
        self._lock.acquire()
        try:
            if filter_str:
                self._keys = list(key for key in sorted(self._dict, key=str.lower)
                                  if regex.findall(key))
            else:
                self._keys = list(sorted(self._dict, key=str.lower))
        finally:
            self._lock.release()
        return True

    def keys(self, filtered=True):
        self._lock.acquire()
        try:
            if filtered:
                return self._keys[:]
            if self._sort:
                return list(sorted(self._dict, key=str.lower))
            return list(self._dict)
        finally:
            self._lock.release()

    def values(self, filtered=True):
        self._lock.acquire()
        try:
            if filtered:
                return list(self._dict[key] for key in self._keys)
            if self._sort:
                return list(self._dict[key] for key in sorted(self._dict, key=str.lower))
            return list(self._dict.values())
        finally:
            self._lock.release()

    def items(self, filtered=True):
        self._lock.acquire()
        try:
            if filtered:
                return list((key, self._dict[key]) for key in self._keys)
            if self._sort:
                return list((key, self._dict[key]) for key in sorted(self._dict, key=str.lower))
            return list(self._dict.items())
        finally:
            self._lock.release()

    def get(self, key, default=None):
        return self._dict.get(key, default)

    def pop(self, key):
        self._lock.acquire()
        try:
            if key in self._keys:
                self._keys.remove(key)
            return self._dict.pop(key)
        finally:
            self._lock.release()

    def size(self, filtered=True):
        if filtered:
            return len(self._keys)
        return len(self._dict)

    def index(self, key, filtered=True):
        self._lock.acquire()
        try:
            if filtered:
                return self._keys.index(key)
            return list(sorted(self._dict, key=str.lower)).index(key.lower())
        except ValueError:
            return -1
        finally:
            self._lock.release()

    def clear(self):
        self._lock.acquire()
        try:
            self._dict = {}
            self._keys = []
        finally:
            self._lock.release()

    def __setitem__(self, key, value):
        self._lock.acquire()
        try:
            if key not in self._dict and (not self._regex or self._regex.findall(key)):
                idx = bisect.bisect(tuple(map(str.lower, self._keys)), key.lower())
                self._keys.insert(idx, key)
            self._dict[key] = value
        finally:
            self._lock.release()

    def __getitem__(self, key):
        return self._dict.__getitem__(key)

    def __contains__(self, key):
        return key in self._dict

    def __len__(self):
        return self.size(filtered=True)
