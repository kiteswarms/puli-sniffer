#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import sys
import time
import threading
import curses
import traceback

from .common import getPrecTime, startThread

MAX_UPDATE_FREQUENCY = 14
RESIZE_DELAY_TICKS = 5 if ("nt" in sys.modules) else 1

# Global display lock
g_display_lock = threading.Lock()

_COLOR_PAIR_MAP = {}


# ------------------------------------------------------------------------------
def registerColor(foreground, background, ident=None):
    if ident is None:
        ident = len(_COLOR_PAIR_MAP) + 1
        while ident in _COLOR_PAIR_MAP:
            ident += 1
    elif ident in _COLOR_PAIR_MAP:
        raise ValueError("ident already in use")
    _COLOR_PAIR_MAP[ident] = (foreground, background)
    return ident


# ------------------------------------------------------------------------------
class App():
    _run = True
    _resize_timer = RESIZE_DELAY_TICKS
    _current_page = None

    def __init__(self):
        self.pages = {}
        # Initialize curses
        self.stdscr = curses.initscr()
        curses.noecho()  # No echo for pressed keys
        curses.cbreak()  # No line buffering
        curses.curs_set(0)  # Hide cursor
        self.stdscr.keypad(1)  # Enable escape sequences
        curses.mousemask(curses.BUTTON1_PRESSED | curses.BUTTON1_RELEASED)
        # Initialize the color map
        curses.start_color()
        for col_id, col_pair in _COLOR_PAIR_MAP.items():
            curses.init_pair(col_id, *col_pair)

    def _exitOnError(self):
        msg = traceback.format_exc()
        self.destroy(ignoreLock=True)
        # with open("log_sniffer_crash.txt", 'w') as out:
        #    out.write(msg + '\n')
        print(msg)

    def addPage(self, name, page):
        self.pages[name] = page
        if len(self.pages) == 1:
            self._current_page = page
            self.reset()

    def getPage(self, name):
        return self.pages.get(name)

    def showPage(self, name):
        page = self.pages.get(name)
        if page:
            self._current_page = page
            self.resize()

    def resize(self):
        try:
            curses.resize_term(0, 0)
        except BaseException:
            pass
        curses.update_lines_cols()
        scrH, scrW = self.stdscr.getmaxyx()
        # Resize the visible display
        try:
            if self._current_page:
                self._current_page.resize(scrH, scrW)
            # Repaint everything
            self.reset()
        except BaseException:
            pass
        self._resize_timer = 0

    def reset(self):
        self.stdscr.touchwin()
        self.stdscr.refresh()
        # Refresh visible window
        if self._current_page:
            self._current_page.reset()

    def addstr(self, win, y, x, txt, col=None, attr=0):
        winH, winW = win.getmaxyx()
        if (x + len(txt)) < winW and y < winH:
            try:
                if col:
                    win.addstr(y, x, txt, curses.color_pair(col) | attr)
                else:
                    win.addstr(y, x, txt)
                return True
            except curses.error:
                pass
        return False

    def __updateLoop(self):
        while self._run:
            ltime = getPrecTime()
            g_display_lock.acquire()
            if not self._run:
                g_display_lock.release()
                break
            try:
                if self._resize_timer > 0:
                    self._resize_timer -= 1
                    if not self._resize_timer:
                        self.resize()
                elif self._current_page:
                    # Update the visible window
                    self._current_page.update()
            except BaseException:
                return self._exitOnError()
            finally:
                g_display_lock.release()
            # Clear the flags
            if self._current_page:
                self._current_page.core.touch()
            # Apply sleep delay adjust
            ctime = getPrecTime()
            delay = max(1.0 / MAX_UPDATE_FREQUENCY - (ctime - ltime), 0.0)
            delay and time.sleep(delay)

    def destroy(self, ignoreLock=False):
        self._run = False
        if (not ignoreLock):
            g_display_lock.acquire()
            g_display_lock.release()
        # Clear the terminal
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()

    def loop(self):
        # Perform an initial resize to scale and refresh the active page
        self.resize()
        # Start background thread
        startThread(self.__updateLoop)
        # Console input handler loop (blocking)
        while self._run:
            try:
                key = self.stdscr.getkey()
            except BaseException:
                break
            if key in ('\x03',):  # 'ctrl + c' results in exit
                return self.destroy()
            g_display_lock.acquire()
            try:
                if key in ("KEY_RESIZE",):
                    # Reset the resize delay counter
                    self._resize_timer = RESIZE_DELAY_TICKS
                # Pass key to currently active window
                elif self._current_page:
                    self._current_page.handleInput(key)
            except BaseException:
                return self._exitOnError()
            finally:
                g_display_lock.release()

    def __del__(self):
        self.destroy()


# ------------------------------------------------------------------------------
class DisplayPage():
    def __init__(self, parent, core):
        self.stdscr = parent.stdscr
        self.parent = parent
        self.core   = core

    def resize(self, scrH, scrW):
        # Update the sizes of content windows
        pass

    def reset(self):
        # Reset the display entirely
        pass

    def update(self):
        # Update the display as needed
        pass

    def addstr(self, win, y, x, txt, col=None, attr=0):
        winH, winW = win.getmaxyx()
        if (x + len(txt)) < winW and y < winH:
            if col:
                win.addstr(y, x, txt, curses.color_pair(col) | attr)
            else:
                win.addstr(y, x, txt)
            return True
        return False

    def handleInput(self, key):
        # Process input events from curses
        pass
