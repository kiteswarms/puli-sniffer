#!/usr/bin/env python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of pulicast.
#
#     pulicast is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     pulicast is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
import pulicast
from pulicast.discovery.node_discoverer import NodeDiscoverer
from pulicast.discovery.node_view import NodeView
from pulicast.discovery.channel_discoverer import ChannelDiscoverer
from pulicast.discovery.channel_view import ChannelView
import argparse
import curses
import string
import pprint
import datetime
import socket

from . import sniffer
from .common import SnifferDisplayPage, SimpleQuoteStripper
from .utils import zcm_utils
from .utils import puli_utils

DEFAULT_PORT = 7667

LINE_WIDTH  = 80
HEADER_SIZE = 4
FOOTER_SIZE = 1

HEADER_TILE_TEXT = "PULI-SNIFFER"

COLOR_DEFAULT             = sniffer.registerColor(curses.COLOR_WHITE, curses.COLOR_BLACK)
COLOR_SELECTED            = sniffer.registerColor(230, 238)  # pale yellow on dark gray
COLOR_HEADER              = sniffer.registerColor( 72, 237)  # blue-green on dark gray
COLOR_HEADER2             = sniffer.registerColor(139, 237)  # lavender on dark gray
COLOR_HEADER_FILTERED     = sniffer.registerColor(214, 237)  # yellow on dark gray
COLOR_HEADER_ALT          = sniffer.registerColor(246, 237)  # light gray on dark gray
COLOR_HEADER_BAR          = sniffer.registerColor(232,  72)  # black on blue-green
COLOR_HEADER_BAR2         = sniffer.registerColor(232, 139)  # black on lavender
COLOR_HEADER_BAR_FILTERED = sniffer.registerColor(232, 214)  # black on yellow
COLOR_FLAGGED             = sniffer.registerColor(251, 124)  # light gray on dark red
COLOR_SELECTED_FLAGGED    = sniffer.registerColor(230, 160)  # black on red
COLOR_DECODER_ERROR       = sniffer.registerColor(curses.COLOR_RED, curses.COLOR_BLACK)
COLOR_FILTER_ERROR        = sniffer.registerColor(196, 237)  # red on dark gray

FILTER_CHARACTERS = string.ascii_letters + string.digits + "!~#_- =/.^$*+?{}[]\\|()"

# Allocate the curses windows globally (curses sometimes hard crashes otherwise)
WINDOW_HEADER = None
WINDOW_MAIN   = None
WINDOW_FOOTER = None


# ------------------------------------------------------------------------------
class SnifferChannelListDisplay(SnifferDisplayPage):
    COLUMN_NAMES     = ("", "Channel Name", "Counter", "Freq (Hz)")
    COLUMN_WIDTH_MAX = (6, 46, 10, 16)
    COLUMN_ALIGNMENT = (1, -1, 1, 1)
    COLUMN_AUTOSIZE  = (0, 0.8, 0.2, 0)
    selected_idx = 0
    _scroll_offset = 0
    _column_size = COLUMN_WIDTH_MAX
    _pressed_index = -1

    color_header_title     = COLOR_HEADER
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def ensureVisible(self, idx):
        winH, _ = WINDOW_MAIN.getmaxyx()
        start = self._scroll_offset
        if len(self.channels) <= winH:
            self._scroll_offset = 0
        elif (self._scroll_offset + winH) > len(self.channels):
            self._scroll_offset = len(self.channels) - winH
        elif idx <= start:
            self._scroll_offset = max(0, idx - 1)
        elif idx >= (start + winH - 1):
            self._scroll_offset = min(idx - winH + 2, len(self.channels) - winH)
        else:
            return False
        return True

    def getMinColumnWidth(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        min_colw = (0,) * len(self.COLUMN_WIDTH_MAX)
        # Draw the channel values line by line
        for info in self.channels.values():
            if info.index < self._scroll_offset or info.index >= (self._scroll_offset + winH):
                continue
            chn_sizes = (1, len(info.name) + 1, len(str(info.counter)) + 1, 1)
            # Update max content size values
            min_colw = tuple(max(i) for i in zip(chn_sizes, min_colw))
        return min_colw

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)
        # Compute column sizes
        if (scrW - 1) < sum(self.COLUMN_WIDTH_MAX):
            fixed  = sum(w for w, f in zip(self.COLUMN_WIDTH_MAX, self.COLUMN_AUTOSIZE) if not f)
            remain = float(scrW - 2 - fixed)
            sizes  = list(self.COLUMN_WIDTH_MAX)
            min_colw = self.getMinColumnWidth()
            for col, frac in enumerate(self.COLUMN_AUTOSIZE):
                if not frac:
                    continue
                calc = int(remain * frac / sum(self.COLUMN_AUTOSIZE))
                sizes[col] = min(sizes[col], max(min_colw[col], calc))
            self._column_size = tuple(sizes)
        else:
            self._column_size = self.COLUMN_WIDTH_MAX

    def reset(self):
        # Get the item last displayed on the detail view
        detailPage = self.parent.getPage("channel_detail")
        if detailPage:
            channel_name = detailPage.popChannel()
            if channel_name:
                self.selected_idx = self.channels.index(channel_name)
                if self.selected_idx == -1:
                    self.selected_idx = 0
            self.selected_idx = max(0, min(self.selected_idx, len(self.channels) - 1))
        # Repaint everything
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.ensureVisible(self.selected_idx)
        self.repaintChannelList()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def repaintHeader(self, bar_only=False):
        title_text  = " - ".join((HEADER_TILE_TEXT, "List View"))
        # Construct header bar text
        header_bar = ""
        for col, text in enumerate(self.COLUMN_NAMES):
            if self.COLUMN_ALIGNMENT[col] == 1:
                header_bar += text.rjust(self._column_size[col], ' ')
            else:
                header_bar += text.ljust(self._column_size[col], ' ')
        header_end = self._getListIndent() + len(header_bar)
        header_bar = header_bar.rjust(header_end, ' ')
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, bar_only=bar_only)

    def repaintChannelList(self, index_list=(), changed_only=False):
        winH, winW = WINDOW_MAIN.getmaxyx()
        maxW = winW - 1
        # Draw the channel values line by line
        for idx, info in enumerate(self.channels.values()):
            if idx < self._scroll_offset or idx >= (self._scroll_offset + winH):
                continue
            if index_list and idx not in index_list:
                continue
            if changed_only and not info.changed():
                continue
            info.touch()
            # Collect the data
            freq_text = "{:.1f}".format(info.frequency)
            cell_data = (str(idx + 1) + ": ", info.name, str(info.counter), freq_text)
            # Construct the channel row
            freq_col_pos = 0
            channel_row = ""
            for col, text in enumerate(cell_data):
                freq_col_pos = len(channel_row) + 1
                if self.COLUMN_ALIGNMENT[col] == 1:
                    channel_row += text.rjust(self._column_size[col], ' ')
                else:
                    channel_row += text.ljust(self._column_size[col], ' ')
            channel_row = channel_row.ljust(LINE_WIDTH, ' ')[:maxW]
            xPos  = self._getListIndent()
            yPos  = idx - self._scroll_offset
            # Draw the row
            color = COLOR_SELECTED if idx == self.selected_idx else None
            color_flag = COLOR_SELECTED_FLAGGED if idx == self.selected_idx else COLOR_FLAGGED
            if info.flagged_reason:
                if self.addstr(WINDOW_MAIN, yPos, xPos, channel_row[:freq_col_pos], col=color):
                    xPos += freq_col_pos
                    self.addstr(WINDOW_MAIN, yPos, xPos, channel_row[freq_col_pos:], col=color_flag)
                    WINDOW_MAIN.touchline(yPos, 1, True)
            elif self.addstr(WINDOW_MAIN, yPos, xPos, channel_row, col=color):
                WINDOW_MAIN.touchline(yPos, 1, True)
        # Repaint the window
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Switch to channel detail (Return)   Clear filter (Escape)   " \
            "Clear list (ctrl + r)"
        if not self.channels.filter:
            footer_color = self.color_header_title
        else:
            footer_color = self.color_header_title_alt
        WINDOW_FOOTER.bkgd(curses.color_pair(footer_color))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def setSelection(self, idx, ensure_visible=True):
        if self.selected_idx == idx:
            return
        old_idx = self.selected_idx
        self.selected_idx = idx
        index_list = None
        if not ensure_visible:
            index_list = (old_idx, idx)
        elif not self.ensureVisible(idx):
            index_list = (old_idx, idx)
        self.repaintChannelList(index_list=index_list)

    def _getListIndent(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        return max(0, int((winW - sum(self._column_size) - 1) / 2))

    def setDisplayFilter(self, filter_str):
        # Find the channel name for the selected channel
        try:
            channel_name = self.channels.keys()[self.selected_idx]
        except BaseException:
            channel_name = None
        self.channels.setFilter(filter_str)
        # Reselect the previously selected channel if not filtered out
        idx = self.channels.index(channel_name)
        if idx >= 0:
            self.selected_idx = idx
        else:
            self.selected_idx = max(0, min(self.selected_idx, len(self.channels) - 1))
        self.ensureVisible(self.selected_idx)
        # Update the display
        self.reset()

    def update(self):
        # Update the header and list if new channels were added
        if self.core.channels_updated:
            self.reset()
        # Repaint the entire list if the frequencies are updated
        elif self.core.frequency_updated:
            self.repaintChannelList()
        else:
            # Update the counter display of channels with new value
            self.repaintChannelList(changed_only=True)

    def handleInput(self, key):
        # 'enter' or 'right-arrow' to switch to detail view
        if key in ('\n', "PADENTER", "KEY_RIGHT", "KEY_B3"):
            # Find the channel info structure for the selected channel
            try:
                channel_name = self.channels.keys()[self.selected_idx]
                detailPage = self.parent.getPage("channel_detail")
                if detailPage and detailPage.setChannel(channel_name):
                    self.parent.showPage("channel_detail")
            except BaseException:
                pass
        elif key in ('\t',):
            self.parent.showPage("node_list")
        elif key in ("KEY_DOWN", "KEY_C2"):
            # Select the next item down
            new_idx = min(len(self.channels) - 1, self.selected_idx + 1)
            self.setSelection(new_idx)
        elif key in ("KEY_UP", "KEY_A2"):
            # Select the next item up
            new_idx = max(0, self.selected_idx - 1)
            self.setSelection(new_idx)
        elif key in ("KEY_NPAGE", "KEY_C3"):
            # Select the next item half the visible list down
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = min(len(self.channels) - 1, self.selected_idx + int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_PPAGE", "KEY_A3"):
            # Select the next item half the visible list up
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = max(0, self.selected_idx - int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_HOME", "KEY_A1"):
            self.setSelection(0)
        elif key in ("KEY_END", "KEY_C1"):
            self.setSelection(len(self.channels) - 1)
        elif key in FILTER_CHARACTERS:
            filter_str = key
            if self.channels.filter:
                filter_str = self.channels.filter + key
            self.setDisplayFilter(filter_str)
        elif key in ("KEY_BACKSPACE", '\x08'):  # 'backspace' to remove last char in filter
            filter_str = ""
            if self.channels.filter:
                filter_str = self.channels.filter[:-1]
            self.setDisplayFilter(filter_str)
        elif key in ('\x1b',):  # 'escape' to clear the filter
            if self.channels.filter:
                self.setDisplayFilter("")
        elif key in ('\x12',):  # 'ctrl + r' results in display reset
            self.selected_idx = 0
            self.core.reset()
        elif key in ("KEY_MOUSE",):
            try:
                _, x, y, _, bstate = curses.getmouse()
                top, left = WINDOW_MAIN.getbegyx()
                winH, winW = WINDOW_MAIN.getmaxyx()
                maxY = top + min(len(self.channels), winH)
                if x < (left + LINE_WIDTH) and y >= top and y < maxY:
                    new_idx = y - top + self._scroll_offset
                    if bstate & curses.BUTTON1_PRESSED:
                        self.setSelection(new_idx, ensure_visible=False)
                        self._pressed_index = new_idx
                    if bstate & curses.BUTTON1_RELEASED:
                        if new_idx == self.selected_idx or self._pressed_index is None:
                            self.setSelection(new_idx, ensure_visible=False)
                            self.showDetailView()
                        self._pressed_index = None
            except BaseException:
                pass
        else:
            # open("debug.txt", "a").write(repr(key) + "\n")
            pass


# ------------------------------------------------------------------------------
class SnifferChannelDetailDisplay(SnifferDisplayPage):
    detail_channel = None
    _pause_message = None
    _repaint_all   = False

    color_header_title     = COLOR_HEADER
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)

    def reset(self):
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.repaintChannelDetail()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def setChannel(self, channel_name):
        # Find the channel info structure for the selected channel
        self.detail_channel = self.channels.get(channel_name)
        self._pause_message = None
        if self.detail_channel is None:
            return False
        return True

    def popChannel(self):
        self._pause_message = None
        if self.detail_channel:
            detail_name = self.detail_channel.name
            self.detail_channel = None
            return detail_name

    def repaintHeader(self, bar_only=False):
        winH, winW = WINDOW_HEADER.getmaxyx()
        maxW = winW - 1
        title_text  = " - ".join((HEADER_TILE_TEXT, "Detail View"))
        # Construct header bar text
        header_bar = ""
        info = self.detail_channel
        header_bar = "   " + self.getDecoderName(info) + "  -  " + info.name
        time_text  = ""
        # Select the timestamp source
        if self._pause_message is not None:
            timestamp, _ = self._pause_message
        elif self.detail_channel.last_messages:
            timestamp, _ = self.detail_channel.last_messages[-1]
        else:
            timestamp = None
        # Format the timestamp for display
        if timestamp is not None:
            time_obj  = datetime.datetime.fromtimestamp(timestamp)
            time_part = time_obj.strftime("%H:%M:%S")
            time_msec = time_obj.microsecond // 1000
            time_text = f"Time: {time_part}.{time_msec:03d}    "
        freq_text  = "Frequency: {:3.1f} Hz ".format(info.frequency)
        right_text = time_text + freq_text
        header_bar = header_bar.ljust(maxW - 3 - len(right_text)) + right_text
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, show_filter=False,
                            force_alt_color=(self._pause_message is not None), bar_only=bar_only)

    def repaintChannelDetail(self):
        WINDOW_MAIN.erase()
        winH, winW = WINDOW_MAIN.getmaxyx()
        message = None
        # Select the data source
        if self._pause_message is not None:
            timestamp, data = self._pause_message
        elif self.detail_channel.last_messages:
            timestamp, data = self.detail_channel.last_messages[-1]
        else:
            data = None
        # Decode the data
        if data is not None:
            message = zcm_utils.GenericDecoder.parseData(data)
        # Draw the field values line by line
        if message:
            # Determine field data types
            if not hasattr(self.detail_channel, "_field_types"):
                decoder = zcm_utils.GenericDecoder.getMessageType(data)
                self.detail_channel._field_types = zcm_utils.getFieldDataTypes(decoder)
            fields = message.__slots__
            field_types = self.detail_channel._field_types
            types_str = []
            for field in fields:
                value = getattr(message, field)
                type_str = field_types.get(field) or "  ?  "
                if type(value) in (list, tuple, bytes):
                    size_str = f"[{len(value)}]"
                    types_str.append(type_str + size_str)
                else:
                    types_str.append(type_str)
            field_max = max(map(len, fields))
            types_max = max(map(len, types_str))
            types_col = field_max + 4
            value_col = field_max + types_max + 8
            value_max = winW - value_col - 1
            index     = 0
            for field, size_str in zip(fields, types_str):
                value = getattr(message, field)
                value_repr = self.getValueRepr(value, value_max)
                self.addstr(WINDOW_MAIN, index, 2, f"{field:>{field_max}}")
                self.addstr(WINDOW_MAIN, index, types_col, f" {size_str:{types_max}} ",
                            col=COLOR_SELECTED)
                first_index = index
                for repr_line in value_repr.split("\n"):
                    self.addstr(WINDOW_MAIN, index, value_col, repr_line[:value_max])
                    if index != first_index:
                        self.addstr(WINDOW_MAIN, index, types_col, " " * (types_max + 2),
                                    col=COLOR_SELECTED)
                    index += 1
        else:
            self.addstr(WINDOW_MAIN, 1, 5, "No message decoder found", col=COLOR_DECODER_ERROR)
        # Repaint the window
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        WINDOW_FOOTER.erase()
        winH, winW = WINDOW_FOOTER.getmaxyx()
        if self._pause_message is None:
            footer_color = self.color_header_title
            pause_text   = "Pause"
        else:
            footer_color = self.color_header_title_alt
            pause_text   = "Resume"
        footer_text = f" Switch to channel list (Backspace)   {pause_text} capture (p)"
        WINDOW_FOOTER.bkgd(curses.color_pair(footer_color))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def getValueRepr(self, value, max_width):
        pp = pprint.PrettyPrinter(width=max_width, compact=True)
        if type(value) in (list, tuple):
            if len(value) and hasattr(value[0], "__slots__"):
                repr_list = list(
                    SimpleQuoteStripper(self.getNestedValueRepr(v, max_width - 2)) for v in value)
                return pp.pformat(repr_list)
            return pp.pformat(list(value))
        elif type(value) is str:
            return pp.pformat(value).replace("\\n", "\\n\n")
        elif type(value) is bytes:
            # Convert bytes to a hex table
            byte_format = "0x{:02X}"
            repr_spacer = " "
            repr_width = len(byte_format.format(0))
            value_hex = list(map(byte_format.format, value))
            num = int(max_width / (repr_width + len(repr_spacer)))
            hex_rows = list(value_hex[i:i + num] for i in range(0, len(value), num))
            return "\n".join(repr_spacer.join(x) for x in hex_rows)
        elif hasattr(value, "__slots__"):
            return self.getNestedValueRepr(value, max_width)
        return repr(value)[:max_width]

    def getNestedValueRepr(self, value, max_width):
        # Get a reduced single line representation of nested message
        parts = []
        for field in value.__slots__:
            parts.append(field + "=")
            field_val = getattr(value, field)
            if type(field_val) in (list, tuple):
                parts.append("[...], ")
            elif type(field_val) is str:
                parts.append(repr(field_val) + ", ")
            elif type(field_val) is bytes:
                parts.append('b"...", ')
            else:
                parts.append(repr(field_val) + ", ")
        value_repr = "<"
        for part in parts:
            if len(value_repr) + len(part) > (max_width - 4):
                value_repr += "...  "
                break
            value_repr += part
        return value_repr[:-2] + ">"

    def getDecoderName(self, info):
        # Get the channel decoder type
        decoder_name = "###"
        if info.last_messages:
            timestamp, message = info.last_messages[-1]
            if not message:
                return decoder_name
            decoder = zcm_utils.GenericDecoder.getMessageType(message)
            if decoder:
                decoder_name = decoder.__module__
                if not decoder_name.endswith('.' + decoder.__name__):
                    decoder_name += '.' + decoder.__name__
        return decoder_name

    def update(self):
        # Refresh the detail view header
        if self._repaint_all:
            self.repaintHeader()
            self.repaintFooter()
            self._repaint_all = False
        elif self.core.frequency_updated:
            self.repaintHeader(bar_only=True)
        # Refresh the detail view if new data was received
        if self.detail_channel.changed():
            self.detail_channel.touch()
            self.repaintChannelDetail()

    def handleInput(self, key):
        # 'backspace', 'escape', 'left-arrow' to go back
        if key in ("KEY_BACKSPACE", '\x08', '\x1b', "KEY_LEFT", "KEY_B1"):
            self.parent.showPage("channel_list")
        elif key in ("p",):
            if self.detail_channel:
                if self._pause_message is None and len(self.detail_channel.last_messages):
                    self._pause_message = self.detail_channel.last_messages[-1]
                else:
                    self._pause_message = None
                self._repaint_all = True


# ------------------------------------------------------------------------------
class SnifferNodeListDisplay(SnifferDisplayPage):
    COLUMN_NAMES     = ("", "Node Name", "Node ID (Hex)", "Host IP  ")
    COLUMN_WIDTH_MAX = (6, 46, 19, 16)
    COLUMN_ALIGNMENT = (1, -1, -1, 1)
    COLUMN_AUTOSIZE  = (0, 0.8, 0.2, 0)
    selected_idx = 0
    _scroll_offset = 0
    _column_size = COLUMN_WIDTH_MAX
    _pressed_index = -1

    color_header_title     = COLOR_HEADER2
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR2
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info = con_info

    def ensureVisible(self, idx):
        winH, _ = WINDOW_MAIN.getmaxyx()
        start = self._scroll_offset
        if len(self.channels) <= winH:
            self._scroll_offset = 0
        elif (self._scroll_offset + winH) > len(self.channels):
            self._scroll_offset = len(self.channels) - winH
        elif idx <= start:
            self._scroll_offset = max(0, idx - 1)
        elif idx >= (start + winH - 1):
            self._scroll_offset = min(idx - winH + 2, len(self.channels) - winH)
        else:
            return False
        return True

    def getMinColumnWidth(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        min_colw = (0,) * len(self.COLUMN_WIDTH_MAX)
        # Draw the channel values line by line
        for info in self.channels.values():
            if info.index < self._scroll_offset or info.index >= (self._scroll_offset + winH):
                continue
            chn_sizes = (1, len(info.name) + 1, len(str(info.session_id)) + 1, 1)
            # Update max content size values
            min_colw = tuple(max(i) for i in zip(chn_sizes, min_colw))
        return min_colw

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)
        # Compute column sizes
        if (scrW - 1) < sum(self.COLUMN_WIDTH_MAX):
            fixed  = sum(w for w, f in zip(self.COLUMN_WIDTH_MAX, self.COLUMN_AUTOSIZE) if not f)
            remain = float(scrW - 2 - fixed)
            sizes  = list(self.COLUMN_WIDTH_MAX)
            min_colw = self.getMinColumnWidth()
            for col, frac in enumerate(self.COLUMN_AUTOSIZE):
                if not frac:
                    continue
                calc = int(remain * frac / sum(self.COLUMN_AUTOSIZE))
                sizes[col] = min(sizes[col], max(min_colw[col], calc))
            self._column_size = tuple(sizes)
        else:
            self._column_size = self.COLUMN_WIDTH_MAX

    def reset(self):
        # Get the item last displayed on the detail view
        detailPage = self.parent.getPage("node_detail")
        if detailPage:
            session_id = detailPage.popNode()
            if session_id is not None:
                self.selected_idx = self.channels.index(str(session_id))
                if self.selected_idx == -1:
                    self.selected_idx = 0
            self.selected_idx = max(0, min(self.selected_idx, len(self.channels) - 1))
        # Repaint everything
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.ensureVisible(self.selected_idx)
        self.repaintNodeList()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def repaintHeader(self, bar_only=False):
        title_text  = " - ".join((HEADER_TILE_TEXT, "Node View"))
        # Construct header bar text
        header_bar = ""
        for col, text in enumerate(self.COLUMN_NAMES):
            if self.COLUMN_ALIGNMENT[col] == 1:
                header_bar += text.rjust(self._column_size[col], ' ')
            else:
                header_bar += text.ljust(self._column_size[col], ' ')
        header_end = self._getListIndent() + len(header_bar)
        header_bar = header_bar.rjust(header_end, ' ')
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, bar_only=bar_only)

    def repaintNodeList(self, index_list=(), changed_only=False):
        winH, winW = WINDOW_MAIN.getmaxyx()
        maxW = winW - 1
        # Draw the channel values line by line
        for idx, info in enumerate(self.channels.values()):
            if idx < self._scroll_offset or idx >= (self._scroll_offset + winH):
                continue
            if index_list and idx not in index_list:
                continue
            if changed_only and not info.changed():
                continue
            info.touch()
            node = info.node
            # Collect the data
            session_id_hex = f"{node.session_id:016X}"
            ip_split  = node.source.ip.rsplit(".", 1)
            ip_align  = '.'.join([ip_split[0], ip_split[1].ljust(3, ' ')])
            cell_data = (str(idx + 1) + ": ", node.name, session_id_hex, ip_align)
            # Construct the channel row
            channel_row = ""
            for col, text in enumerate(cell_data):
                if self.COLUMN_ALIGNMENT[col] == 1:
                    channel_row += text.rjust(self._column_size[col], ' ')
                else:
                    channel_row += text.ljust(self._column_size[col], ' ')
            channel_row = channel_row.ljust(LINE_WIDTH, ' ')[:maxW]
            xPos  = self._getListIndent()
            yPos  = idx - self._scroll_offset
            # Draw the row
            color = COLOR_SELECTED if idx == self.selected_idx else None
            if self.addstr(WINDOW_MAIN, yPos, xPos, channel_row, col=color):
                WINDOW_MAIN.touchline(yPos, 1, True)
        # Repaint the window
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Switch to node detail (Return)   Clear filter (Escape)   " \
            "Show channels (Tab)"
        if not self.channels.filter:
            footer_color = self.color_header_title
        else:
            footer_color = self.color_header_title_alt
        WINDOW_FOOTER.bkgd(curses.color_pair(footer_color))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def setSelection(self, idx, ensure_visible=True):
        if self.selected_idx == idx:
            return
        old_idx = self.selected_idx
        self.selected_idx = idx
        index_list = None
        if not ensure_visible:
            index_list = (old_idx, idx)
        elif not self.ensureVisible(idx):
            index_list = (old_idx, idx)
        self.repaintNodeList(index_list=index_list)

    def _getListIndent(self):
        winH, winW = WINDOW_MAIN.getmaxyx()
        return max(0, int((winW - sum(self._column_size) - 1) / 2))

    # def setDisplayFilter(self, filter_str):
    #     # Find the channel name for the selected channel
    #     try:
    #         channel_name = self.channels.keys()[self.selected_idx]
    #     except BaseException:
    #         channel_name = None
    #     self.channels.setFilter(filter_str)
    #     # Reselect the previously selected channel if not filtered out
    #     idx = self.channels.index(channel_name)
    #     if idx >= 0:
    #         self.selected_idx = idx
    #     else:
    #         self.selected_idx = max(0, min(self.selected_idx, len(self.channels) - 1))
    #     self.ensureVisible(self.selected_idx)
    #     # Update the display
    #     self.reset()

    def update(self):
        # Update the header and list if new channels were added
        if self.core.channels_updated:
            self.reset()
        # # Repaint the entire list if the frequencies are updated
        # elif self.core.frequency_updated:
        #     self.repaintNodeList()
        else:
            # Update the counter display of channels with new value
            self.repaintNodeList(changed_only=True)

    def handleInput(self, key):
        # 'enter' or 'right-arrow' to switch to detail view
        if key in ('\n', "PADENTER", "KEY_RIGHT", "KEY_B3"):
            # Find the channel info structure for the selected channel
            try:
                session_id = self.channels.values()[self.selected_idx].session_id
                detailPage = self.parent.getPage("node_detail")
                if detailPage and detailPage.setNode(session_id):
                    self.parent.showPage("node_detail")
            except BaseException:
                pass
        elif key in ('\t',):
            self.parent.showPage("channel_list")
        elif key in ("KEY_DOWN", "KEY_C2"):
            # Select the next item down
            new_idx = min(len(self.channels) - 1, self.selected_idx + 1)
            self.setSelection(new_idx)
        elif key in ("KEY_UP", "KEY_A2"):
            # Select the next item up
            new_idx = max(0, self.selected_idx - 1)
            self.setSelection(new_idx)
        elif key in ("KEY_NPAGE", "KEY_C3"):
            # Select the next item half the visible list down
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = min(len(self.channels) - 1, self.selected_idx + int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_PPAGE", "KEY_A3"):
            # Select the next item half the visible list up
            winH, _ = WINDOW_MAIN.getmaxyx()
            full_step = min(len(self.channels), winH)
            new_idx = max(0, self.selected_idx - int(full_step / 2))
            self.setSelection(new_idx)
        elif key in ("KEY_HOME", "KEY_A1"):
            self.setSelection(0)
        elif key in ("KEY_END", "KEY_C1"):
            self.setSelection(len(self.channels) - 1)
        # elif key in FILTER_CHARACTERS:
        #     filter_str = key
        #     if self.channels.filter:
        #         filter_str = self.channels.filter + key
        #     self.setDisplayFilter(filter_str)
        # elif key in ("KEY_BACKSPACE", '\x08'):  # 'backspace' to remove last char in filter
        #     filter_str = ""
        #     if self.channels.filter:
        #         filter_str = self.channels.filter[:-1]
        #     self.setDisplayFilter(filter_str)
        # elif key in ('\x1b',):  # 'escape' to clear the filter
        #     if self.channels.filter:
        #         self.setDisplayFilter("")
        elif key in ("KEY_MOUSE",):
            try:
                _, x, y, _, bstate = curses.getmouse()
                top, left = WINDOW_MAIN.getbegyx()
                winH, winW = WINDOW_MAIN.getmaxyx()
                maxY = top + min(len(self.channels), winH)
                if x < (left + LINE_WIDTH) and y >= top and y < maxY:
                    new_idx = y - top + self._scroll_offset
                    if bstate & curses.BUTTON1_PRESSED:
                        self.setSelection(new_idx, ensure_visible=False)
                        self._pressed_index = new_idx
                    if bstate & curses.BUTTON1_RELEASED:
                        if new_idx == self.selected_idx or self._pressed_index is None:
                            self.setSelection(new_idx, ensure_visible=False)
                            self.showDetailView()
                        self._pressed_index = None
            except BaseException:
                pass
        else:
            # open("debug.txt", "a").write(repr(key) + "\n")
            pass


# ------------------------------------------------------------------------------
class SnifferNodeDetailDisplay(SnifferDisplayPage):
    detail_node_info = None
    _repaint_all   = False

    color_header_title     = COLOR_HEADER2
    color_header_title_alt = COLOR_HEADER_FILTERED
    color_header_bar       = COLOR_HEADER_BAR2
    color_header_bar_alt   = COLOR_HEADER_BAR_FILTERED
    color_header_info      = COLOR_HEADER_ALT
    color_filter_error     = COLOR_FILTER_ERROR

    def __init__(self, parent, core, con_info):
        SnifferDisplayPage.__init__(self, parent, core)
        self.channels = core.channels
        self.con_info  = con_info

    def resize(self, scrH, scrW):
        WINDOW_HEADER.resize(HEADER_SIZE, scrW - 1)
        WINDOW_MAIN.resize(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.resize(FOOTER_SIZE, scrW - 1)
        WINDOW_FOOTER.mvwin(scrH - FOOTER_SIZE, 0)

    def reset(self):
        self.repaintHeader()
        WINDOW_MAIN.erase()
        self.repaintNodeDetail()
        WINDOW_FOOTER.erase()
        self.repaintFooter()

    def setNode(self, session_id):
        # Find the channel info structure for the selected channel
        self.detail_node_info = self.channels.get(str(session_id))
        if self.detail_node_info is None:
            return False
        return True

    def popNode(self):
        if self.detail_node_info:
            session_id = self.detail_node_info.session_id
            self.detail_node_info = None
            return session_id

    def repaintHeader(self, bar_only=False):
        winH, winW = WINDOW_HEADER.getmaxyx()
        maxW = winW - 1
        title_text  = " - ".join((HEADER_TILE_TEXT, "Node Detail"))
        # Construct header bar text
        header_bar = ""
        info = self.detail_node_info
        node = info.node
        header_bar = f"   Pulicast Node - '{node.name}'"
        time_text  = ""
        # Format the timestamp for display
        time_obj  = datetime.datetime.fromtimestamp(info.timestamp)
        time_part = time_obj.strftime("%H:%M:%S")
        time_msec = time_obj.microsecond // 1000
        time_text = f"Time: {time_part}.{time_msec:03d} "
        header_bar = header_bar.ljust(maxW - 3 - len(time_text)) + time_text
        # Use the generic header draw method
        self._repaintHeader(WINDOW_HEADER, title_text, header_bar, show_filter=False,
                            force_alt_color=False, bar_only=bar_only)

    def repaintNodeDetail(self):
        WINDOW_MAIN.erase()
        winH, winW = WINDOW_MAIN.getmaxyx()
        node = self.detail_node_info.node
        # Draw the node information section
        fields = {
            "Session Id": node.session_id,
            "Source Ip": node.source.ip,
            "Source Port": node.source.port,
            "Heartbeat": node.heartbeat_period,
            "Subscriptions": list(node.subscriptions),
            "Publications": list(node.publications.keys()),
        }
        field_max = max(map(len, fields.keys()))
        value_col = field_max + 4
        value_max = winW - value_col - 1
        index     = 0
        for field, value in fields.items():
            value_repr = self.getValueRepr(value, value_max)
            self.addstr(WINDOW_MAIN, index, 0, f"  {field:>{field_max}} ",
                        col=COLOR_SELECTED)
            first_index = index
            for repr_line in value_repr.split("\n"):
                self.addstr(WINDOW_MAIN, index, value_col, repr_line[:value_max])
                if index != first_index:
                    self.addstr(WINDOW_MAIN, index, 0, " " * (field_max + 3),
                                col=COLOR_SELECTED)
                index += 1
        # Repaint the window
        WINDOW_MAIN.refresh()

    def repaintFooter(self):
        WINDOW_FOOTER.erase()
        winH, winW = WINDOW_FOOTER.getmaxyx()
        footer_text = " Switch to node list (Backspace)"
        WINDOW_FOOTER.bkgd(curses.color_pair(self.color_header_title))
        self.addstr(WINDOW_FOOTER, 0, 0, footer_text[:(winW - 1)])
        WINDOW_FOOTER.refresh()

    def getValueRepr(self, value, max_width):
        pp = pprint.PrettyPrinter(width=max_width, compact=True)
        if type(value) in (list, tuple):
            return pp.pformat(list(value))
        elif type(value) is str:
            return pp.pformat(value).replace("\\n", "\\n\n")
        return repr(value)[:max_width]

    def update(self):
        # Refresh the detail view header
        if self._repaint_all:
            self.repaintHeader()
            self.repaintFooter()
            self._repaint_all = False
        # Refresh the detail view if new data was received
        # if self.detail_node_info.changed():
        #     self.detail_node_info.touch()
        self.repaintNodeDetail()

    def handleInput(self, key):
        # 'backspace', 'escape', 'left-arrow' to go back
        if key in ("KEY_BACKSPACE", '\x08', '\x1b', "KEY_LEFT", "KEY_B1"):
            self.parent.showPage("node_list")


# ------------------------------------------------------------------------------
def main():
    global WINDOW_HEADER, WINDOW_MAIN, WINDOW_FOOTER
    # Parse the commandline arguments
    description = "Terminal based spy utility. Subscribes to all channels on a pulicast transport" \
                  " and displays them in an interactive terminal."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-p", "--port", default=pulicast.DEFAULT_PORT, type=int,
                        help="Pulicast capture port")
    parser.add_argument("-i", "--interface", default="0.0.0.0", type=str,
                        help="Pulicast capture interface ip address")
    parser.add_argument("-d", "--decoder-path", default=["zcom"], type=str, action='append',
                        help="Path to (or name of) modules/package with message decoder objects")
    parser.add_argument("-n", "--node", default=False, action='store_true',
                        help="Start the sniffer with the node information view")
    args = parser.parse_args()

    # Construct connection info string
    con_info = "port: " + str(args.port) + " | interface: " + args.interface

    # Load message decoder
    for path in args.decoder_path:
        zcm_utils.loadMessageDecoder(path)

    # Create and start a pulicast socket node
    node_name = "{} ({})".format(HEADER_TILE_TEXT.lower(), socket.gethostname())
    puli = pulicast.ThreadedNode(node_name, port=args.port, interface_address=args.interface)
    # Start the background thread for package processing
    puli.start()

    # Create cores and add data sources
    all_channel_core = sniffer.Core()
    meta_info_core = puli_utils.PuliNodeCore()

    # Create channel discoverer to find all channels in the network
    node_discoverer = NodeDiscoverer(puli)
    channel_discoverer = ChannelDiscoverer(node_discoverer)

    def on_new_channel(channel: ChannelView):
        # Ignore channels that were already found and ignore internal messages
        if channel.name in puli or channel.name.startswith("__"):
            return

        # Bytes callback mapping to the classic call signature with channel name
        def channel_callback(msg: bytes):
            all_channel_core.handleMessage(channel.name, msg)
        # Subscribe to the new channel
        puli[channel.name].subscribe(channel_callback)

    def on_new_node(node: NodeView):
        meta_info_core.handleNode(str(node.session_id), node)

    def on_del_node(node: NodeView):
        meta_info_core.removeNode(str(node.session_id))

    channel_discoverer.add_on_item_added_callback(on_new_channel)
    node_discoverer.add_on_item_added_callback(on_new_node)
    node_discoverer.add_on_item_removed_callback(on_del_node)

    # Create the main app
    app = sniffer.App()
    # Create the curses windows for the views (to avoid curses crashes in some instances)
    scrH, scrW = app.stdscr.getmaxyx()
    WINDOW_HEADER = curses.newwin(HEADER_SIZE, scrW - 1, 0, 0)
    WINDOW_HEADER.bkgd(curses.color_pair(COLOR_HEADER))
    WINDOW_MAIN = curses.newwin(scrH - HEADER_SIZE - FOOTER_SIZE, scrW - 1, HEADER_SIZE, 0)
    WINDOW_MAIN.bkgd(curses.color_pair(COLOR_DEFAULT))
    WINDOW_FOOTER = curses.newwin(FOOTER_SIZE, scrW - 1, scrH - FOOTER_SIZE, 0)

    # Create the view objects
    app.addPage("channel_list", SnifferChannelListDisplay(app, all_channel_core, con_info))
    app.addPage("channel_detail", SnifferChannelDetailDisplay(app, all_channel_core, con_info))

    app.addPage("node_list", SnifferNodeListDisplay(app, meta_info_core, con_info))
    app.addPage("node_detail", SnifferNodeDetailDisplay(app, meta_info_core, con_info))

    # Start with the node list view if '-m/--meta' trigger is given
    if args.node:
        app.showPage("node_list")
    # Start main loop - wait for exit command on stdin
    app.loop()

    # Cleanup
    app.destroy()
    puli.stop()

    return 0
