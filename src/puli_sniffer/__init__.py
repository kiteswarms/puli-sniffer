from importlib_metadata import version, PackageNotFoundError

try:
    __version__ = version("puli-sniffer")
except PackageNotFoundError:
    # package is not installed
   raise
