from setuptools import setup, find_packages
import os

requirements = [
    "importlib_metadata",
    "pulicast"]

if os.name == "nt":
    requirements.append("windows-curses")

setup(
    name="puli-sniffer",
    author="Nicholas Feix",
    author_email="nf@fconsoft.com",
    description="sniffer utilities to inspect packages in a pulicast network.",
    url='https://gitlab.com/kiteswarms/puli-sniffer',
    setup_requires=["setuptools_scm", "setuptools"],
    use_scm_version=True,
    install_requires=requirements,
    entry_points="""
        [console_scripts]
        puli-sniffer = puli_sniffer.sniffer_main:main
        puli-log-sniffer = puli_sniffer.log_sniffer_main:main
        puli-state-sniffer = puli_sniffer.state_sniffer_main:main
        """,
    python_requires=">=3.6",
    package_dir={"": "src"},
    packages=find_packages("src"),
    license="GPLv3",
    long_description="More detailed description",
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
