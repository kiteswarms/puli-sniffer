#!/usr/bin/python3
import sys

from puli_sniffer.state_sniffer_main import main


if __name__ == "__main__":
    sys.exit(main())
