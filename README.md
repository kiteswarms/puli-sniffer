# What is this?
- Terminal based sniffer for pulicast communication
- contains two tools:
  - puli-sniffer for viewing all pulicast messages sorted by channel names
  - puli-log-sniffer for viewing 'log' messages sorted by origin field

# How to get it?
```sh
pip3 install puli-sniffer
```

# How to use it?
## puli-sniffer
```sh
puli-sniffer
```
## puli-log-sniffer
```sh
puli-log-sniffer
```

## startup arguments
- `-p PORT, --port PORT`: Pulicast capture port number
- `-i IP, --interface IP`: Ip address of the network interface pulicast should be bound to.
- `-d PATH, --decoder-path PATH`: Path to (or name of) modules/package with message decoder objects.

> The default `interface` value is `0.0.0.0` which usually selects the first available interface.
> This highly depends on the users network structure.  
> We advise to either manually configure routing for the pulicast ip range to an adapter or using the `interface` switch to bind to one.
